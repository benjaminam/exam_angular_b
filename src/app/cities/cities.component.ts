import { CityService } from './../city.service';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';
import { Observable } from 'rxjs';

@Component({
  selector: 'cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})

export class CitiesComponent implements OnInit {

  cities:Object[] = [{id:1, name:'London'},{id:2, name:'Paris'},{id:3, name:'Tel Aviv'},{id:4, name:'Jerusalem'},{id:5, name:'Berlin'},{id:6, name:'Rome'},{id:7, name:'Dubai'},{id:8, name:'Athens'}];
  city:string;
  predictResult:string;
  displayedColumns: string[] = ['Name', 'getData','temperature','humidity','prediction'];
  temperature:number;
  humidity:number;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  result$:Observable<string>;
  temp:string;
  hum:string;
  predi:string;
  image:string

getData(city){
  this.city=this.route.snapshot.params.city;
    this.weatherData$=this.WeatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.humidity = data.humidity;
        this.image = data.image;

      },
      error=>{
        console.log(error.message);
        this.hasError= true;
        this.errorMessage=error.message;
      }
    )
   
}


  constructor(private route:ActivatedRoute, private WeatherService:WeatherService,private CityService:CityService) { }
  predict(){
    this.result$ = this.CityService.predictRain(this.temp,this.hum);
    this.result$.subscribe(
      res => {
        this.predictResult = res;
        if(Number(res) > 50){
          this.predi = 'rain tomorrow';
        }else{
          this.predi = 'no rain tomorrow';
        }
      }
    )
  }
  
  ngOnInit(): void {
    
  }


  }

