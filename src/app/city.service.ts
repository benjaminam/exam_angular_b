import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  cityCollection:AngularFirestoreCollection = this.db.collection('cities');

  URL:string = "https://7jntcj7i6f.execute-api.us-east-1.amazonaws.com/default";

  constructor(private http:HttpClient,private db:AngularFirestore) { }

  predictRain(temp,hum){
    const body = {
      data:{
        temp:temp,
        hum:hum,
      }
    }
    return this.http.post<any>(this.URL,body).pipe(
      map(res => {
        const final:string = res.body;
        console.log({final});
        return final;
      })
    )
  }

  saveCity(temp:number,hum:number,predict:string){
    const city = {
      temp:temp,
      hum:hum,
      predict:predict,
    }
  }
}
