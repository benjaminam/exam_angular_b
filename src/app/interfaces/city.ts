export interface City {
    id:number,
    name:string,
    temperature?:string;
    humidity?:string
}
