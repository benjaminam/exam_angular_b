export interface Weather {
    name:string,
    temperature:number,
    image:string,

    humidity:number,

}
