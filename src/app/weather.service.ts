import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Weather } from './interfaces/weather';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "86d979390316df3c12488c8da5e7f633";
  private IMP = "units=metric";

  constructor(private http:HttpClient) { }
    searchWeatherData(cityName:string):Observable<Weather>{
      return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
        map(data => this.transformWeatherData(data)),
        catchError(this.handleError)
      )
    }

    private handleError(res:HttpErrorResponse){
      console.log(res.error);
      return throwError(res.error || 'Server Error');
    }

    private transformWeatherData(data:WeatherRaw):Weather{
      return {
        name:data.name,
        temperature:data.main.temp,
        humidity:data.main.temp,
        image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,


      }
    }
    
    }