import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ByeComponent } from './bye/bye.component';
import { CitiesComponent } from './cities/cities.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [ 
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'welcome', component: WelcomeComponent },
  { path: 'bye', component: ByeComponent },
  { path: 'cities', component: CitiesComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
