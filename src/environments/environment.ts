// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBi6paOeqeNj7BH55Azus-dW1cn2hLFric",
    authDomain: "moed-b-angular.firebaseapp.com",
    projectId: "moed-b-angular",
    storageBucket: "moed-b-angular.appspot.com",
    messagingSenderId: "1044888759431",
    appId: "1:1044888759431:web:63415b8a088a3c133b1c22"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
